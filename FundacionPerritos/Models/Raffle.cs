﻿namespace FundacionPerritos.Models
{
    public class Raffle
    {

        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public string Key { get; set; }

    }
}