﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FundacionPerritos.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index() => View();
        public ActionResult Main() => View();
    }
}