﻿using FundacionPerritos.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace FundacionPerritos.Controllers
{
    public class RafflesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Raffles
        public IQueryable<Raffle> GetRaffles()
        {
            return db.Raffles;
        }

        // GET: api/Raffles/5
        [ResponseType(typeof(Raffle))]
        public IHttpActionResult GetRaffle(int id)
        {
            Raffle raffle = db.Raffles.Find(id);
            if (raffle == null)
            {
                return NotFound();
            }

            return Ok(raffle);
        }

        // PUT: api/Raffles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRaffle(int id, Raffle raffle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != raffle.Id)
            {
                return BadRequest();
            }

            db.Entry(raffle).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RaffleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Raffles
        [ResponseType(typeof(Raffle))]
        public IHttpActionResult PostRaffle(Raffle raffle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Raffles.AddOrUpdate(raffle);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = raffle.Id }, raffle);
        }

        // DELETE: api/Raffles/5
        [ResponseType(typeof(Raffle))]
        public IHttpActionResult DeleteRaffle(int id)
        {
            Raffle raffle = db.Raffles.Find(id);
            if (raffle == null)
            {
                return NotFound();
            }

            db.Raffles.Remove(raffle);
            db.SaveChanges();

            return Ok(raffle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RaffleExists(int id)
        {
            return db.Raffles.Count(e => e.Id == id) > 0;
        }
    }
}