﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FundacionPerritos.Startup))]
namespace FundacionPerritos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
