namespace FundacionPerritos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Raffles", "Key", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Raffles", "Key");
        }
    }
}
