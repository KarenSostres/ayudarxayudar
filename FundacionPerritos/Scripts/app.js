//Configuracion de Angular

var app = angular.module("funtacionPerritos", [
    "ngRoute",
    "ngResource",
    "toastr",
    "720kb.tooltips",
    "angular-loading-bar",
    "angularUtils.directives.dirPagination"
]);

//LOCAL

var uriApi = `${window
    .location
    .protocol
    .replace(/:/g, "")}://localhost:44345`,
    successfullyMessage = "Datos guardados",
    errorMessage = "Ocurrio un error! El servidor dejo de responder, intente de nuevo.";

//Vistas

app.config(function ($routeProvider) {
    $routeProvider
        .when("/Main", { templateUrl: "Home/Main" })
        .when("/Verify", { templateUrl: "Home/Vefiry" })
        .when("/Create", { templateUrl: "Home/Create" })
        .when("/Error", { templateUrl: "Home/Error" })
        .otherwise({ templateUrl: "Main" });
});

// Toaster

app.config(function (toastrConfig) {
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: "toast-container",
        maxOpened: 0,
        newestOnTop: true,
        positionClass: "toast-bottom-right",
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: "body"
    });
});


app.controller("main-controller", ($scope, $http, toastr) => {

    $http.get(`${uriApi}/api/Raffles`).then((response) => {
        $scope.raffles = response.data;
    }).catch((error) => {
        toastr.error(errorMessage, "Error");
    });


    $scope.saveNumber = () => {
        if ($scope.newNumber.$valid) {
            $http.post(`${uriApi}/api/Raffles`, {
                Id: $scope.idNum,
                Number: $scope.number,
                Name: $scope.name,
                Phone: $scope.phone,
                Status: 'Open',
                Key: '1234'
            }).then((response) => {
                toastr.success(successfullyMessage, "Info");
                $scope.idNum = 0;
                $scope.number = null;
                $scope.name = null
                $scope.phone = null
                $http.get(`${uriApi}/api/Raffles`).then((response) => {
                    $scope.raffles = response.data;
                }).catch((error) => {
                    toastr.error(errorMessage, "Error");
                });
            }).catch((error) => {
                snackbar(errorMessage);;
            });

        }
    };


    $scope.selectNumber = (id) => {
        $http.get(`${uriApi}/api/Raffles/${id}`).then((response) => {
            $scope.idNum = response.data.Id;
            $scope.number = response.data.Number;
            $scope.name = response.data.Name;
            $scope.phone = response.data.Phone;
        }).catch((error) => {
            snackbar(errorMessage);;
        });
    };


    $scope.deleteNumber = (id) => {
        Swal.fire({
            title: "Eliminar",
            text: `Seguro que deseas eliminar el registro?`,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.value) {
                $http.delete(`${uriApi}/api/Raffles/${id}`).then((response) => {
                    $scope.number = null;
                    $scope.name = null
                    $scope.phone = null
                    $http.get(`${uriApi}/api/Raffles/`).then((response) => {
                        $scope.raffles = response.data;
                        Swal.fire(
                            'Eliminado',
                            `Registro sido eliminado`,
                            'success'
                        )
                    }).catch((error) => {
                        toastr.error(errorMessage, "Error");
                    });

                }).catch((error) => {
                    toastr.error(errorMessage, "Error");
                });
            }

        })

    };


});

